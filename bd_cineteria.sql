drop database if exists bd_cineteria;
create database bd_cineteria;
use bd_cineteria;

create table clasificacion_pelicula(
id_clasificacion int not null primary key auto_increment,
clasificacion varchar(150),
detalle varchar(150)
);
create table tipo_tecnologia(
id_tipo int not null primary key auto_increment,
nombre varchar(15)
);
create table peliculas(
id_pelicula int not null primary key auto_increment,
nombre varchar(150),
duracion varchar(150),
id_tipo int,
id_clasificacion int,
director varchar(150),
detalle varchar(150),
foreign key(id_tipo) references tipo_tecnologia(id_tipo),
foreign key(id_clasificacion) references clasificacion_pelicula(id_clasificacion)
);

create table salas(
id_sala int not null primary key auto_increment,
nombre varchar(150),
id_tipo int,
num_asientos int,
foreign key(id_tipo) references tipo_tecnologia(id_tipo)
);

create table asientos(
id_asiento int not null primary key auto_increment,
nombre_asiento int,
id_sala int,
estado enum('Reservado','Disponible'),
foreign key(id_sala) references salas(id_sala)
);

create table horario(
id_horario int not null primary key auto_increment,
fecha_hora_emision date,
id_sala int,
id_pelicula int,
foreign key(id_sala) references salas(id_sala),
foreign key(id_pelicula) references peliculas(id_pelicula)
);

create table tipo_turno(
id_tipo_turno int not null primary key auto_increment,
turno enum('matutino', 'vespertino', 'extra'),
detalle varchar(150)
);
create table rol(
id_rol int not null primary key auto_increment,
nombre varchar(150)
);
create table persona(
id_persona int not null primary key auto_increment,
nombre varchar(150),
apellido varchar(150),
dui varchar(150),
fecha_nacimiento date,
direccion varchar(150),
telefono varchar(150)
);


create table turno_empleado(
id_turno int not null primary key auto_increment,
id_tipo_turno int,
id_empleado int,
foreign key(id_tipo_turno) references tipo_turno(id_tipo_turno)
);

create table empleado(
id_empleado int not null primary key auto_increment,
id_persona int,
id_rol int,
id_turno int,
foreign key(id_persona) references persona(id_persona),
foreign key(id_rol) references rol(id_rol),
foreign key(id_turno) references turno_empleado (id_turno)
);

create table tipo_boleto(
id_tipo_boleto int not null primary key auto_increment,
tipo varchar(150),
precio decimal
);


create table factura(
id_factura int not null primary key auto_increment,
fecha date,
id_pelicula int,
total_pagar decimal
);

create table boleto(
id_boleto int not null primary key auto_increment,
id_tipo_boleto int,
id_sala int,
id_factura int,
foreign key(id_tipo_boleto) references tipo_boleto(id_tipo_boleto),
foreign key(id_sala) references salas(id_sala),
foreign key(id_factura) references factura(id_factura)
);

create table detalle_factura(
id_detalle_factura int not null primary key auto_increment,
id_tipo_boleto int,
cantidad int,
foreign key(id_tipo_boleto) references tipo_boleto(id_tipo_boleto)
);

create table inf_cine(
id_cine int not null primary key auto_increment,
nombre varchar(150),
ubicacion varchar(150),
telefono varchar(150),
correo varchar(150)
);

