/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cineteria.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author alvaro.bautistausam
 */
public class GenericDao {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("connPU");//nombre de la persistencia
    EntityManager em = emf.createEntityManager();

    public Object insertar(Object obj) {
        try {
            em.persist(obj);
            em.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;

    }

    public boolean modificar(Object obj) {
        try {
            em.merge(obj);
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
