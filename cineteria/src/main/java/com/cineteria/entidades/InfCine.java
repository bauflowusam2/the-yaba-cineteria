/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cineteria.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alvaro.bautistausam
 */
@Entity
@Table(name = "inf_cine")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InfCine.findAll", query = "SELECT i FROM InfCine i")
    , @NamedQuery(name = "InfCine.findByIdCine", query = "SELECT i FROM InfCine i WHERE i.idCine = :idCine")
    , @NamedQuery(name = "InfCine.findByNombre", query = "SELECT i FROM InfCine i WHERE i.nombre = :nombre")
    , @NamedQuery(name = "InfCine.findByUbicacion", query = "SELECT i FROM InfCine i WHERE i.ubicacion = :ubicacion")
    , @NamedQuery(name = "InfCine.findByTelefono", query = "SELECT i FROM InfCine i WHERE i.telefono = :telefono")
    , @NamedQuery(name = "InfCine.findByCorreo", query = "SELECT i FROM InfCine i WHERE i.correo = :correo")})
public class InfCine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cine")
    private Integer idCine;
    @Size(max = 150)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 150)
    @Column(name = "ubicacion")
    private String ubicacion;
    @Size(max = 150)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 150)
    @Column(name = "correo")
    private String correo;

    public InfCine() {
    }

    public InfCine(Integer idCine) {
        this.idCine = idCine;
    }

    public Integer getIdCine() {
        return idCine;
    }

    public void setIdCine(Integer idCine) {
        this.idCine = idCine;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCine != null ? idCine.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InfCine)) {
            return false;
        }
        InfCine other = (InfCine) object;
        if ((this.idCine == null && other.idCine != null) || (this.idCine != null && !this.idCine.equals(other.idCine))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cineteria.entidades.InfCine[ idCine=" + idCine + " ]";
    }
    
}
