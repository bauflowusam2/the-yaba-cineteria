/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cineteria.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvaro.bautistausam
 */
@Entity
@Table(name = "tipo_turno")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoTurno.findAll", query = "SELECT t FROM TipoTurno t")
    , @NamedQuery(name = "TipoTurno.findByIdTipoTurno", query = "SELECT t FROM TipoTurno t WHERE t.idTipoTurno = :idTipoTurno")
    , @NamedQuery(name = "TipoTurno.findByTurno", query = "SELECT t FROM TipoTurno t WHERE t.turno = :turno")
    , @NamedQuery(name = "TipoTurno.findByDetalle", query = "SELECT t FROM TipoTurno t WHERE t.detalle = :detalle")})
public class TipoTurno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_turno")
    private Integer idTipoTurno;
    @Size(max = 10)
    @Column(name = "turno")
    private String turno;
    @Size(max = 150)
    @Column(name = "detalle")
    private String detalle;
    @OneToMany(mappedBy = "idTipoTurno")
    private List<TurnoEmpleado> turnoEmpleadoList;

    public TipoTurno() {
    }

    public TipoTurno(Integer idTipoTurno) {
        this.idTipoTurno = idTipoTurno;
    }

    public Integer getIdTipoTurno() {
        return idTipoTurno;
    }

    public void setIdTipoTurno(Integer idTipoTurno) {
        this.idTipoTurno = idTipoTurno;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    @XmlTransient
    public List<TurnoEmpleado> getTurnoEmpleadoList() {
        return turnoEmpleadoList;
    }

    public void setTurnoEmpleadoList(List<TurnoEmpleado> turnoEmpleadoList) {
        this.turnoEmpleadoList = turnoEmpleadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoTurno != null ? idTipoTurno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoTurno)) {
            return false;
        }
        TipoTurno other = (TipoTurno) object;
        if ((this.idTipoTurno == null && other.idTipoTurno != null) || (this.idTipoTurno != null && !this.idTipoTurno.equals(other.idTipoTurno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cineteria.entidades.TipoTurno[ idTipoTurno=" + idTipoTurno + " ]";
    }
    
}
