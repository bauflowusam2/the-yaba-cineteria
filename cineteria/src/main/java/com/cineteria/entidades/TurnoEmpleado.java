/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cineteria.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvaro.bautistausam
 */
@Entity
@Table(name = "turno_empleado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TurnoEmpleado.findAll", query = "SELECT t FROM TurnoEmpleado t")
    , @NamedQuery(name = "TurnoEmpleado.findByIdTurno", query = "SELECT t FROM TurnoEmpleado t WHERE t.idTurno = :idTurno")
    , @NamedQuery(name = "TurnoEmpleado.findByIdEmpleado", query = "SELECT t FROM TurnoEmpleado t WHERE t.idEmpleado = :idEmpleado")})
public class TurnoEmpleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_turno")
    private Integer idTurno;
    @Column(name = "id_empleado")
    private Integer idEmpleado;
    @JoinColumn(name = "id_tipo_turno", referencedColumnName = "id_tipo_turno")
    @ManyToOne
    private TipoTurno idTipoTurno;
    @OneToMany(mappedBy = "idTurno")
    private List<Empleado> empleadoList;

    public TurnoEmpleado() {
    }

    public TurnoEmpleado(Integer idTurno) {
        this.idTurno = idTurno;
    }

    public Integer getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(Integer idTurno) {
        this.idTurno = idTurno;
    }

    public Integer getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public TipoTurno getIdTipoTurno() {
        return idTipoTurno;
    }

    public void setIdTipoTurno(TipoTurno idTipoTurno) {
        this.idTipoTurno = idTipoTurno;
    }

    @XmlTransient
    public List<Empleado> getEmpleadoList() {
        return empleadoList;
    }

    public void setEmpleadoList(List<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTurno != null ? idTurno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TurnoEmpleado)) {
            return false;
        }
        TurnoEmpleado other = (TurnoEmpleado) object;
        if ((this.idTurno == null && other.idTurno != null) || (this.idTurno != null && !this.idTurno.equals(other.idTurno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cineteria.entidades.TurnoEmpleado[ idTurno=" + idTurno + " ]";
    }
    
}
