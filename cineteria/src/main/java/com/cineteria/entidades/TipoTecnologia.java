/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cineteria.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvaro.bautistausam
 */
@Entity
@Table(name = "tipo_tecnologia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoTecnologia.findAll", query = "SELECT t FROM TipoTecnologia t")
    , @NamedQuery(name = "TipoTecnologia.findByIdTipo", query = "SELECT t FROM TipoTecnologia t WHERE t.idTipo = :idTipo")
    , @NamedQuery(name = "TipoTecnologia.findByNombre", query = "SELECT t FROM TipoTecnologia t WHERE t.nombre = :nombre")})
public class TipoTecnologia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo")
    private Integer idTipo;
    @Size(max = 15)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(mappedBy = "idTipo")
    private List<Peliculas> peliculasList;
    @OneToMany(mappedBy = "idTipo")
    private List<Salas> salasList;

    public TipoTecnologia() {
    }

    public TipoTecnologia(Integer idTipo) {
        this.idTipo = idTipo;
    }

    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Peliculas> getPeliculasList() {
        return peliculasList;
    }

    public void setPeliculasList(List<Peliculas> peliculasList) {
        this.peliculasList = peliculasList;
    }

    @XmlTransient
    public List<Salas> getSalasList() {
        return salasList;
    }

    public void setSalasList(List<Salas> salasList) {
        this.salasList = salasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipo != null ? idTipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoTecnologia)) {
            return false;
        }
        TipoTecnologia other = (TipoTecnologia) object;
        if ((this.idTipo == null && other.idTipo != null) || (this.idTipo != null && !this.idTipo.equals(other.idTipo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cineteria.entidades.TipoTecnologia[ idTipo=" + idTipo + " ]";
    }
    
}
