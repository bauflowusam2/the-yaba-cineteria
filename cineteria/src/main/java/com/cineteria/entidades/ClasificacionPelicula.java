/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cineteria.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvaro.bautistausam
 */
@Entity
@Table(name = "clasificacion_pelicula")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClasificacionPelicula.findAll", query = "SELECT c FROM ClasificacionPelicula c")
    , @NamedQuery(name = "ClasificacionPelicula.findByIdClasificacion", query = "SELECT c FROM ClasificacionPelicula c WHERE c.idClasificacion = :idClasificacion")
    , @NamedQuery(name = "ClasificacionPelicula.findByClasificacion", query = "SELECT c FROM ClasificacionPelicula c WHERE c.clasificacion = :clasificacion")
    , @NamedQuery(name = "ClasificacionPelicula.findByDetalle", query = "SELECT c FROM ClasificacionPelicula c WHERE c.detalle = :detalle")})
public class ClasificacionPelicula implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_clasificacion")
    private Integer idClasificacion;
    @Size(max = 150)
    @Column(name = "clasificacion")
    private String clasificacion;
    @Size(max = 150)
    @Column(name = "detalle")
    private String detalle;
    @OneToMany(mappedBy = "idClasificacion")
    private List<Peliculas> peliculasList;

    public ClasificacionPelicula() {
    }

    public ClasificacionPelicula(Integer idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public Integer getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(Integer idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    @XmlTransient
    public List<Peliculas> getPeliculasList() {
        return peliculasList;
    }

    public void setPeliculasList(List<Peliculas> peliculasList) {
        this.peliculasList = peliculasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idClasificacion != null ? idClasificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClasificacionPelicula)) {
            return false;
        }
        ClasificacionPelicula other = (ClasificacionPelicula) object;
        if ((this.idClasificacion == null && other.idClasificacion != null) || (this.idClasificacion != null && !this.idClasificacion.equals(other.idClasificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cineteria.entidades.ClasificacionPelicula[ idClasificacion=" + idClasificacion + " ]";
    }
    
}
