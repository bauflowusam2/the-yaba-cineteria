/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cineteria.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alvaro.bautistausam
 */
@Entity
@Table(name = "tipo_boleto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoBoleto.findAll", query = "SELECT t FROM TipoBoleto t")
    , @NamedQuery(name = "TipoBoleto.findByIdTipoBoleto", query = "SELECT t FROM TipoBoleto t WHERE t.idTipoBoleto = :idTipoBoleto")
    , @NamedQuery(name = "TipoBoleto.findByTipo", query = "SELECT t FROM TipoBoleto t WHERE t.tipo = :tipo")
    , @NamedQuery(name = "TipoBoleto.findByPrecio", query = "SELECT t FROM TipoBoleto t WHERE t.precio = :precio")})
public class TipoBoleto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_boleto")
    private Integer idTipoBoleto;
    @Size(max = 150)
    @Column(name = "tipo")
    private String tipo;
    @Column(name = "precio")
    private Long precio;
    @OneToMany(mappedBy = "idTipoBoleto")
    private List<DetalleFactura> detalleFacturaList;
    @OneToMany(mappedBy = "idTipoBoleto")
    private List<Boleto> boletoList;

    public TipoBoleto() {
    }

    public TipoBoleto(Integer idTipoBoleto) {
        this.idTipoBoleto = idTipoBoleto;
    }

    public Integer getIdTipoBoleto() {
        return idTipoBoleto;
    }

    public void setIdTipoBoleto(Integer idTipoBoleto) {
        this.idTipoBoleto = idTipoBoleto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    @XmlTransient
    public List<DetalleFactura> getDetalleFacturaList() {
        return detalleFacturaList;
    }

    public void setDetalleFacturaList(List<DetalleFactura> detalleFacturaList) {
        this.detalleFacturaList = detalleFacturaList;
    }

    @XmlTransient
    public List<Boleto> getBoletoList() {
        return boletoList;
    }

    public void setBoletoList(List<Boleto> boletoList) {
        this.boletoList = boletoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoBoleto != null ? idTipoBoleto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoBoleto)) {
            return false;
        }
        TipoBoleto other = (TipoBoleto) object;
        if ((this.idTipoBoleto == null && other.idTipoBoleto != null) || (this.idTipoBoleto != null && !this.idTipoBoleto.equals(other.idTipoBoleto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cineteria.entidades.TipoBoleto[ idTipoBoleto=" + idTipoBoleto + " ]";
    }
    
}
